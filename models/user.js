const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    var User = sequelize.define('User', {
        saguid: {
            type: Sequelize.STRING(16)
        },
        program_id: {
            type: Sequelize.STRING(16)
        },
        status: {
            type: Sequelize.STRING(16)
        },
        archive_data: {
            type: Sequelize.STRING
        },
        last_modified: {
            type: Sequelize.DATE
        }
    }, {
        //options
        // getterMethods
        // setterMethods

        // don't add the timestamp attributes (updatedAt, createdAt)
        // in default of Sequlize, setting, created columns that I didn't make like 'createdAt', 'uupdateAt'
        timestamps: false
    })

    // TODO : primay key: saguid+last_modified
    User.sync();
};