
const Container = require('typedi').Container;
module.exports = {
    add: ({ saguid, programId, status, last_modified }) => {
        let userModel = Container.get('User');
        try {
            return userModel.create({
                saguid: saguid,
                program_id: programId,
                status: status,
                last_modified: last_modified
            });
        } catch (e) {
            throw new Error(e);
        }
    },
    setStatus: ({ saguid, programId, status }) => {
        // TODO : check invalid status
        // [
        //     'sa_leave',
        //     'invalid_admin',
        //     'pending_admin',
        //     'dormant',
        //     'pre-dormant'
        // ]

        let userModel = Container.get('User');
        try {
            return userModel.findOne({
                where: {
                    saguid: saguid,
                    program_id: programId
                }
            }).then(user => {
                console.log('setStatus user: ', user);
                if (!!user) {
                    console.log('setStatus user.save()');
                    user.status = status;
                    return user.save();
                } else {
                    console.log('setStatus user.create()');
                    return userModel.create({
                        saguid: saguid,
                        program_id: programId,
                        status: status,
                        last_modified: new Date()
                    });
                }
            });
        } catch (e) {
            throw new Error(e);
        }
    },

    search: ({ saguid, programId, status }) => {
        let userModel = Container.get('User');
        let filter = {};
        if (!!saguid) {
            filter.saguid = saguid;
        }
        if (!!programId) {
            filter.program_id = programId;
        }
        if (!!status) {
            filter.status = status;
        }
        try {
            return userModel.findAll({
                where: filter
            }).then(result => {
                if (result.length === 0) {
                    throw new Error('No Users: cannot search the user.');
                } else {
                    return result;
                }
            })
        } catch (e) {
            throw new Error(e);
        }
    },

    delete: ({ saguid, programId }) => {
        let userModel = Container.get('User');
        try {
            return userModel.findOne({
                where: {
                    saguid: saguid,
                    program_id: programId
                }
            }).then(user => {
                if (user === null) {
                    throw new Error('No Users: cannot delete the user.');
                } else {
                    return user.destroy();
                }
            })
        } catch (e) {
            throw new Error(e);
        }
    },
}