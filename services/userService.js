const Container = require('typedi').Container;

//TODO: Where to put USER_STATUS
const USER_STATUS = {
     SA_LEAVE: 'sa_leave',
     INVALID_ADMIN: 'invalid_admin',
     PENDING_ADMIN: 'pending_admin',
     DORMANT: 'dormant',
     PRE_DORMANT: 'pre-dormant'
 }

class UserService {
    constructor() {
        this.events = Container.get('EventEmitter');
        this.userModel = Container.get('User');
    }
    setPreDormantStatus(saguid) {
        console.log('services/user.js, userService, setPreDormantStatus');
        //TODO: Search and get user by saguid
        this.userModel.saguid = saguid;
        this.events.emit('setUserStatus', this.userModel, USER_STATUS.PRE_DORMANT);
    }

    setDormantStatus(saguid) {
        console.log('services/user.js, userService, setDormantStatus');
        //TODO: Search and get user by saguid
        this.userModel.saguid = saguid;
        this.events.emit('getAndStoreArchiveData', this.userModel);
        this.events.emit('setUserStatus', this.userModel, USER_STATUS.DORMANT);
    }

    setSALeave(saguid) {
        console.log('services/user.js, userService, setSALeave');
        //TODO: Search and get user by saguid
        this.userModel.saguid = saguid;
        this.events.emit('setUserStatus', this.userModel, USER_STATUS.SA_LEAVE);
    }

    searchUser(query) {
        console.log('services/user.js, userService, searchUser');
        this.events.emit('searchUser', query);
    }

    deleteUser(saguid) {
        console.log('services/user.js, userService, deleteUser');
        this.events.emit('deleteUser', saguid);
    }
    //Dev code
    addUser(saguid, status) {
        console.log('services/user.js, userService, addUser');
        console.log('services/user.js, userService, addUser: ', saguid, status);
        this.userModel.create({
            saguid: saguid,
            status: status
        });
    }
}

console.log('services/userService');
Container.set('UserService', new UserService());