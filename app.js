const express = require('express');

async function startServer() {
    let config = {};
    config.port = 3000;
    const app = express();

    const loader = require('./loaders');
    await loader(app);
    
    //TODO: Get config
    app.listen(config.port, err => {
        if (err) {
            console.error(err); //TODO: Use logger
            process.exit(1);
            return;
        }
        console.log(`
            ################################################
            🛡️  Server listening on port: ${config.port} 🛡️ 
            ################################################
        `);
    });

    process.on('uncaughtException', (err) => {
        console.error('There was an uncaught error', err)
        process.exit(1) //mandatory (as per the Node docs)
    })
}

startServer();