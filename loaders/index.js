const expressLoader = require('./express');
const sequelizeLoader = require('./db');
const diLoader = require('./di');
const logger = require('./logger');

module.exports = async (app) => {
    const sequelize = await sequelizeLoader();
    const userService = require('../service/user')
    await diLoader({
        sequelize,
        userService
    });
    await expressLoader(app, logger);
}