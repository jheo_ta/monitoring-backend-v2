const routes = require('../api');
const bodyParser = require('body-parser');

module.exports = async (app, logger) => {
    console.log('loaders/express.js', app);
    app.use(bodyParser.json());
    app.use('/hc', (req, res) => {
        res.status(200).send('OK');
    });

    if (logger) {
        app.use((req, res, next) => {
            req.logger = logger;
            req.logger.log(`[${req.method}]`, req.originalUrl, "body", req.body);
            next();
        })
    }

    // Load API routes
    routes(app);

    /// catch 404 and forward to error handler
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    /// error handlers
    app.use((err, req, res, next) => {
        if (err.name === 'UnauthorizedError') {
            return res
                .status(err.status)
                .send({
                    message: err.message
                })
                .end();
        }
        return next(err);
    });

    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });

}