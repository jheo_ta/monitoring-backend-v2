/*
    Logger
    TODO: will come logger from @sjim
*/

module.exports = {
    log: (...args) => {
        console.log('✔', ...args);
    },
    error: (...args) => {
        console.log('♨', ...args);
    },
    critical: (...args) => {
        console.log('✋', ...args);
    }
};;