/*
    Dependency injector
    In this code, we are injecting what we need to use in services
*/
const Container = require('typedi').Container;

module.exports = async ({ sequelize, userService }) => {
    Container.set('User', sequelize.models.User);
    Container.set('UserService', userService);
    // add others if you need
}