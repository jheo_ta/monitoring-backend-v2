const Sequelize = require('sequelize');
const userModel = require('../models/user');

module.exports = async () => {
    // const sequelize = new Sequelize('test', 'root', 'tecacedev', {
    //     host: 'localhost',
    //     dialect: 'mysql',
    //     pool: {
    //         max: 20,
    //         min: 0,
    //         acquire: 300000,
    //         idle: 1000
    //     }
    // });

    const sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: 'test/database.sqlite',
        pool: { // TODO: be updated
            max: 20,
            min: 0,
            acquire: 300000,
            idle: 1000
        }
    });
    userModel(sequelize)
    return sequelize;
}