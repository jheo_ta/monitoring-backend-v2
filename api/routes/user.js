/*
    Create a router and call functions that are needed to process API
    If router gets an error, it passes to root router and the error handler works on it
*/
const express = require('express');
const router = express.Router();
const Container = require('typedi').Container;

router.get('/', async (req, res, next) => {
    let userService = Container.get('UserService');
    try {
        // TODO: custom validator (other endpoints as wll)
        const result = await userService.search(req.query);
        return res.status(200).send(result);
    } catch (e) {
        console.log(e);
        req.logger.error('🔥 error: ', e);
        return next(e)
    }
})

router.post('/status', async (req, res, next) => {
    let userService = Container.get('UserService');
    try {
        await userService.setStatus({ ...req.body, last_modified: new Date() });
        return res.status(200).end();
    } catch (e) {
        req.logger.error('🔥 error: ', e.toString());
        return next(e)
    }
})

router.post('/', async (req, res, next) => {
    let userService = Container.get('UserService');
    try {
        await userService.add({ ...req.body, last_modified: new Date() });
        return res.status(201).end();
    } catch (e) {
        req.logger.error('🔥 error: ', e);
        return next(e)
    }
})

router.delete('/', async (req, res, next) => {
    let userService = Container.get('UserService');
    try {
        await userService.delete({...req.body});
        return res.status(200).end();
    } catch (e) {
        req.logger.error('🔥 error: ', e);
        return next(e)
    }
})

module.exports = router;