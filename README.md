# Samsung Developer Site Monitoring System V2 #

This repository is the Samsung Developer Site Monitoring System V2

Version: V2

### Architect ###
* [Bulletproof node.js project architecture](https://softwareontheroad.com/ideal-nodejs-project-structure/#pubsub)

### Project Structure ###
Name  | Description
------------- | -------------
models/User.js | MySQL or Sqlite(dev) schema and model for Samsung Account User
app.js | App entry point
api/Index.js | Express route controllers for all the endpoints of the app
api/routes/user.js | Routes: user api
config | Environment variables and configuration related stuff
config/default.json | Local configuration
config/dev.json | Dev configuration
config/production.json | Production configuration
loaders | Split the startup process into modules
loaders/db.js | Init sequelize(lib) with mysql
loaders/di.js | Set container to inject later by typeDI lib with db(sequelize instance) and event emitter
loaders/error.js | Custom error class
loaders/express.js | Middlewares: logger, body parser, ...
loaders/logger.js | Create a logger instance
services | All the business logic is here
services/user.js | User Backend APIs
models/user.js | User schema
tests/routes.test.js | Testing the end points
tests/database.sqlite | Sqlite db for local develop

### List of Packages ###
Package  | Description
------------- | -------------
sequelize | A promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server.
express-validator | A library of string validators and sanitizers.

### Reference ###
* [Hackathon Starter](https://github.com/sahat/hackathon-starter)